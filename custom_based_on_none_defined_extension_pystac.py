# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0
from typing import Any, Dict, Literal, Union

import pystac
from pystac.extensions.base import (
    ExtensionManagementMixin,
    PropertiesExtension,
)
from pystac.utils import get_required, map_opt

CONTACTS = "contacts"
# contact
NAME = "name"
ORGANIZATION = "organization"
IDENTIFIER = "identifier"
EMAILS = "emails"
PHONES = "phones"
POSITION = "position"
LOGO = "logo"
ADDRESSES = "addresses"
LINKS = "links"
CONTACTINSTRUCTIONS = "contactInstructions"
ROLES = "roles"

# Info
VALUE = "value"
ROLES = "roles"

# Address
DELIVERYPOINT = "deliveryPoint"
CITY = "city"
ADMINISTRATIVEAREA = "administrativeArea"
POSTALCODE = "postalCode"
COUNTRY = "country"

# Link
HREF = "href"
REL = "rel"
TYPE = "type"
TITLE = "title"


class Info:
    properties: Dict[str, Any]

    def __init__(self, properties: Dict[str, Any]) -> None:
        self.properties = properties

    @property
    def value(self) -> str:
        """The column name"""
        return get_required(self.properties.get(VALUE), self, VALUE)

    @value.setter
    def value(self, v: str) -> None:
        self.properties[VALUE] = v

    @property
    def roles(self) -> list | None:
        """Detailed multi-line description to explain the column. `CommonMark 0.29
        <http://commonmark.org/>`__ syntax MAY be used for rich text representation.
        """
        return self.properties.get(ROLES)

    @roles.setter
    def roles(self, v: list | None) -> None:
        if v is None:
            self.properties.pop(ROLES, None)
        else:
            self.properties[ROLES] = v

    def to_dict(self) -> dict[str, Any]:
        """Returns a dictionary representing this ``Table``."""
        return self.properties

    @staticmethod
    def from_dict(d: dict[str, str]) -> "Info":
        return Info(d.get("value"), d.get("roles"))  # type: ignore


class Address:
    properties: Dict[str, Any]

    def __init__(self, properties: Dict[str, Any]) -> None:
        self.properties = properties

    @property
    def deliveryPoint(self) -> list[str] | None:
        """The column name"""
        return self.properties.get(DELIVERYPOINT)

    @deliveryPoint.setter
    def deliveryPoint(self, v: list[str]) -> None:
        self.properties[DELIVERYPOINT] = v

    @property
    def city(self):
        """The column name"""
        return self.properties.get(CITY)

    @city.setter
    def city(self, v: str) -> None:
        self.properties[CITY] = v

    @property
    def administrativeArea(self):
        return self.properties.get(ADMINISTRATIVEAREA)

    @administrativeArea.setter
    def administrativeArea(self, v: str) -> None:
        self.properties[ADMINISTRATIVEAREA] = v

    @property
    def postalCode(self):
        """The column name"""
        return self.properties.get(POSTALCODE)

    @postalCode.setter
    def postalCode(self, v: str) -> None:
        self.properties[POSTALCODE] = v

    @property
    def country(self):
        """The column name"""
        return self.properties.get(COUNTRY)

    @country.setter
    def country(self, v: str) -> None:
        self.properties[COUNTRY] = v

    def to_dict(self) -> dict[str, Any]:
        """Returns a dictionary representing this ``Table``."""
        return self.properties

    @staticmethod
    def from_dict(d: dict[str, str]) -> "Address":
        return Address(  # type: ignore
            d.get("deliveryPoint"),  # type: ignore
            d.get("city"),
            d.get("administrativeArea"),
            d.get("postalCode"),
            d.get("country"),
        )


class Link:
    properties: Dict[str, Any]

    def __init__(self, properties: Dict[str, Any]) -> None:
        self.properties = properties

    @property
    def href(self) -> str:
        return get_required(self.properties.get(HREF), self, HREF)

    @href.setter
    def href(self, v: str) -> None:
        self.properties[HREF] = v

    @property
    def rel(self) -> str:
        return get_required(self.properties.get(REL), self, REL)

    @rel.setter
    def rel(self, v: str) -> None:
        self.properties[REL] = v

    @property
    def type(self):
        return self.properties.get(TYPE)

    @type.setter
    def type(self, v: str) -> None:
        self.properties[TYPE] = v

    @property
    def title(self):
        """The column name"""
        return self.properties.get(TITLE)

    @title.setter
    def title(self, v: str) -> None:
        self.properties[TITLE] = v

    def to_dict(self) -> dict[str, Any]:
        """Returns a dictionary representing this ``Table``."""
        return self.properties

    @staticmethod
    def from_dict(d: dict[str, str]) -> "Info":
        return Info(d.get("href"), d.get("rel"), d.get("type"), d.get("title"))  # type: ignore


class Contact:
    properties: dict[str, str]

    def __init__(self, properties) -> None:
        self.properties = properties

    @property
    def name(self) -> str | None:
        return get_required(self.properties.get(NAME), self, NAME)

    @name.setter
    def name(self, v: str) -> None:
        self.properties[NAME] = v

    @property
    def organization(self) -> str | None:
        return get_required(self.properties.get(ORGANIZATION), self, NAME)

    @organization.setter
    def organization(self, v: str) -> None:
        self.properties[ORGANIZATION] = v

    @property
    def identifier(self) -> str | None:
        return self.properties.get(IDENTIFIER)

    @identifier.setter
    def identifier(self, v: str) -> None:
        self.properties[IDENTIFIER] = v

    @property
    def position(self) -> str | None:
        return self.properties.get(POSITION)

    @position.setter
    def position(self, v: str) -> None:
        self.properties[POSITION] = v

    @property
    def logo(self) -> Link | None:
        return map_opt(Link.from_dict, self.properties.get(LOGO))

    @logo.setter
    def logo(self, v: Link | None) -> None:
        self.properties[LOGO] = map_opt(lambda link: link.to_dict(), v)

    @property
    def phones(self) -> list[Info] | None:
        return map_opt(
            lambda phones: [Info.from_dict(phone) for phone in phones],
            self.properties.get(PHONES),
        )

    @phones.setter
    def phones(self, v: list[Info] | None) -> None:
        self.properties[PHONES] = map_opt(
            lambda phones: [phone.to_dict() for phone in phones], v
        )

    @property
    def emails(self) -> list[Info] | None:
        return map_opt(
            lambda emails: [Info.from_dict(email) for email in emails],
            self.properties.get(EMAILS),
        )

    @emails.setter
    def emails(self, v: list[Info] | None) -> None:
        self.properties[EMAILS] = map_opt(
            lambda emails: [email.to_dict() for email in emails], v
        )

    @property
    def addresses(self) -> list[Address] | None:
        return map_opt(
            lambda addresses: [
                Address.from_dict(address) for address in addresses
            ],
            self.properties.get(ADDRESSES),
        )

    @addresses.setter
    def addresses(self, v: list[Address] | None) -> None:
        self.properties[ADDRESSES] = map_opt(
            lambda addresses: [address.to_dict() for address in addresses], v
        )

    @property
    def links(self) -> list[Link] | None:
        return map_opt(
            lambda links: [Link.from_dict(link) for link in links],
            self.properties.get(LINKS),
        )

    @links.setter
    def links(self, v: list[Link] | None) -> None:
        self.properties[LINKS] = map_opt(
            lambda links: [link.to_dict() for link in links], v
        )

    @property
    def contactInstructions(self) -> str | None:
        return self.properties.get(CONTACTINSTRUCTIONS)

    @contactInstructions.setter
    def contactInstructions(self, v: str) -> None:
        self.properties[CONTACTINSTRUCTIONS] = v

    @property
    def roles(self) -> str | None:
        return self.properties.get(ROLES)

    @roles.setter
    def roles(self, v: str) -> None:
        self.properties[ROLES] = v

    def to_dict(self) -> dict[str, Any]:
        """Returns a dictionary representing this ``Table``."""
        return self.properties

    @staticmethod
    def from_dict(d: dict[str, str]) -> "Contact":
        return Contact(  # type: ignore
            d.get("name"),
            d.get("organization"),
            d.get("identifier"),
            d.get("position"),
            d.get("logo"),
            d.get("phones"),
            d.get("emails"),
            d.get("addresses"),
            d.get("links"),
            d.get("contactInstructions"),
            d.get("roles"),
        )


SCHEMA_URI: str = (
    "https://stac-extensions.github.io/contacts/v0.1.1/schema.json"
)


class ContactsExtension(
    PropertiesExtension,
    ExtensionManagementMixin[Union[pystac.Item, pystac.Collection]],
):
    name: Literal["contacts"] = "contacts"
    obj: pystac.STACObject

    def __init__(self, item: pystac.Item):
        self.item = item
        self.properties = item.properties

    @classmethod
    def get_schema_uri(cls) -> str:
        return SCHEMA_URI

    def apply(
        self,
        contacts: list[Contact],
    ) -> None:
        """Applies scientific extension properties to the extended
        :class:`~pystac.Item`.

        Args:
            doi : Optional DOI string for the item.  Must not be a DOI link.
            citation : Optional human-readable reference.
            publications : Optional list of relevant publications
                referencing and describing the data.
        """
        self.contacts = contacts

    @property
    def contacts(self) -> list[Contact] | None:
        return map_opt(
            lambda conts: [Contact.from_dict(cont) for cont in conts],
            self._get_property(CONTACTS, list[dict[str, Any]]),
        )

    @contacts.setter
    def contacts(self, v: list[Contact] | None) -> None:
        self._set_property(
            CONTACTS,
            map_opt(lambda conts: [cont.to_dict() for cont in conts], v),
        )

    @classmethod
    def ext(
        cls, obj: pystac.Item, add_if_missing: bool = False
    ) -> "ContactsExtension":
        if isinstance(obj, pystac.Item):
            cls.validate_has_extension(obj, add_if_missing)
            return ContactsExtension(obj)
        else:
            raise pystac.ExtensionTypeError(
                f"ContactExtension does not apply to type '{type(obj).__name__}'"
            )


# item = pystac.read_file(
#     "https://raw.githubusercontent.com/radiantearth/stac-spec/master/examples/core-item.json"
# )
# order_ext = ContactExtension.ext(item, add_if_missing=True)

# order_ext.name = "Mo"
# order_ext.organization = "KIT"
# order_ext.identifier = "123"
# order_ext.position = "PhD"
# order_ext.logo = Link({"href": "https://example.com/logo.png", "rel": "logo"})
# order_ext.phones = [Info({"value": "123", "roles": ["work"]})]
# order_ext.emails = [Info({"value": "123", "roles": ["work"]})]
# order_ext.addresses = [Address({"deliveryPoint": "123", "city": "Karlsruhe", "administrativeArea": "Baden-Württemberg", "postalCode": "76131", "country": "Germany"})]
# order_ext.links = [Link({"href": "https://example.com/logo.png", "rel": "logo"})]
# order_ext.contactInstructions = "Please contact me"
# order_ext.roles = ["author"]

# order_ext.apply(
#     name="Mo",
#     organization="KIT",
#     identifier="123",
#     position="PhD",
#     logo=Link({"href": "https://codebase.helmholtz.cloud/uploads/-/system/group/avatar/16928/cat4kit__1_.png?width=80", "rel": "logo"}),
#     phones=[Info({"value": "123", "roles": ["work"]})],
#     emails=[Info({"value": "123", "roles": ["work"]})],
#     addresses=[Address({"deliveryPoint": "123", "city": "Karlsruhe", "administrativeArea": "Baden-Württemberg", "postalCode": "76131", "country": "Germany"})],
#     links=[Link({"href": "https://example.com/logo.png", "rel": "logo"})],
#     contactInstructions="Please contact me",
#     roles=["author"],
# )
# pprint(item.properties)


def item(item, harvesting_vars):
    contacts_ = ContactsExtension.ext(item, add_if_missing=True)

    link = Link(
        {
            "href": harvesting_vars["logo_href"],
            "rel": harvesting_vars["logo_rel"],
            "type": harvesting_vars["logo_type"],
            "title": harvesting_vars["logo_title"],
        }
    ).to_dict()
    phones = [
        Info(
            {
                "value": harvesting_vars["phones_value"],
                "roles": harvesting_vars["phones_roles"],
            }
        ).to_dict()
    ]
    emails = [
        Info(
            {
                "value": harvesting_vars["emails_value"],
                "roles": harvesting_vars["emails_roles"],
            }
        ).to_dict()
    ]
    contacts_.apply(
        contacts=[
            Contact(
                {
                    "name": harvesting_vars["name"],
                    "organization": harvesting_vars["organization"],
                    "identifier": harvesting_vars["identifier"],
                    "position": harvesting_vars["position"],
                    "logo": link,
                    "phones": phones,
                    "emails": emails,
                    "addresses": [
                        Address(
                            {
                                "deliveryPoint": [
                                    harvesting_vars["addresses_deliveryPoint"]
                                ],
                                "city": harvesting_vars["addresses_city"],
                                "administrativeArea": harvesting_vars[
                                    "addresses_administrativeArea"
                                ],
                                "postalCode": harvesting_vars[
                                    "addresses_postalCode"
                                ],
                                "country": harvesting_vars[
                                    "addresses_country"
                                ],
                            }
                        ).to_dict()
                    ],
                    "links": [
                        Link(
                            {
                                "href": harvesting_vars["links_href"],
                                "rel": harvesting_vars["links_rel"],
                                "type": harvesting_vars["logo_type"],
                            }
                        ).to_dict()
                    ],
                    "contactInstructions": harvesting_vars[
                        "contactInstructions"
                    ],
                    "roles": harvesting_vars["roles"],
                }
            )
        ]
        # name=harvesting_vars["name"],
        # organization=harvesting_vars["organization"],
        # identifier=harvesting_vars["identifier"],
        # position=harvesting_vars["position"],
        # logo=Link({"href": harvesting_vars["logo_href"], "rel": harvesting_vars["logo_rel"]}),
        # phones=[Info({"value": harvesting_vars["phones_value"], "roles": harvesting_vars["phones_roles"]})],
        # emails=[Info({"value": harvesting_vars["emails_value"], "roles": harvesting_vars["emails_roles"]})],
        # addresses=[Address({"deliveryPoint": harvesting_vars["addresses_deliveryPoint"], "city": harvesting_vars["addresses_city"], "administrativeArea": harvesting_vars["addresses_administrativeArea"], "postalCode": harvesting_vars["addresses_postalCode"], "country": harvesting_vars["addresses_country"]})],
        # links=[Link({"href": harvesting_vars["links_href"], "rel": harvesting_vars["links_rel"]})],
        # contactInstructions=harvesting_vars["contactInstructions"],
        # roles=harvesting_vars["roles"],
    )


# item = pystac.read_file(
#     "https://raw.githubusercontent.com/radiantearth/stac-spec/master/examples/core-item.json"
# )


# order_ext = OrderExtension.ext(item, add_if_missing=True)

# # Create a unique string ID for the order ID
# order_ext.order_id = str(uuid4())

# # Create some fake order history and set it using the extension
# event_1 = OrderEvent.create(
#     event_type=OrderEventType.SUBMITTED, timestamp=datetime.now() - timedelta(days=1)
# )
# event_2 = OrderEvent.create(
#     event_type=OrderEventType.STARTED_PROCESSING,
#     timestamp=datetime.now() - timedelta(hours=12),
# )
# event_3 = OrderEvent.create(
#     event_type=OrderEventType.DELIVERED, timestamp=datetime.now() - timedelta(hours=1)
# )
# order_ext.history = [event_1, event_2, event_3]

# class Scientific:
#     def item(self, item, harvesting_vars):
#         item_publication = []
#         item_publication = [
#             Publication(
#                 harvesting_vars["publications"][0],
#                 harvesting_vars["publications"][1],
#             )
#         ]
#         scientific = ScientificExtension.ext(item, add_if_missing=True)
#         scientific.apply(
#                 doi=harvesting_vars['doi'],
#                 citation=harvesting_vars['citation'],
#                 publications=item_publication,
#         )

# def item(item, harvesting_vars):
#     item_publication = []
#     item_publication = [
#         Publication(
#             harvesting_vars["publications"][0],
#             harvesting_vars["publications"][1],
#         )
#     ]
#     scientific = ScientificExtension.ext(item, add_if_missing=True)
#     scientific.apply(
#             doi=harvesting_vars['doi'],
#             citation=harvesting_vars['citation'],
#             publications=item_publication,
#     )


# TDS2STACIntegrator("https://thredds.imk-ifu.kit.edu/thredds/catalog/regclim/raster/global/era5/sfc/single/daily/catalog.html",
#          stac_dir="/Users/hadizadeh-m/stac/",
#          limited_number = 2,
#          depth_number = 1,
#          webservice_properties={"webservice": "all",
#                                 "web_service_config_file":"./tag_example.json",
#                                 },

#          extension_properties={"item_extensions": ["common_metadata", "item_datacube_extension", ("scientific_extension", "item", "/Users/hadizadeh-m/dev/20231102/tds2stac/custom_based_on_defined_extension_pystac.py"), ("contact_extension", "item", "/Users/hadizadeh-m/dev/20231102/tds2stac/custom_based_on_none_defined_extension_pystac.py")],
#                                },
#         asset_properties={"item_thumbnail": True,
#                           "item_getminmax_thumbnail": True,
#                           "explore_data": True,
#                           "verify_explore_data": True,
#                             "jupyter_notebook": True,
#                           "collection_thumbnail": "wms",
#                           "assets_list": ["wms", "wcs", "wfs"],})
