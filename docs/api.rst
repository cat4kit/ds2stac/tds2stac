..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0




======================
API Reference
======================

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: API Reference

   api/tds2stac
   api/*


This API reference is auto-generated from the Python docstrings. The table of contents
on the left is organized by module. The section right is sub sections of this page.
