..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


==============
Installation
==============

.. warning:: This page has been automatically generated as has not yet been reviewed
   by the authors of tds2stac!


To install the *tds2stac* package, we recommend that you install it from
PyPi via

.. code:: bash

   pip install tds2stac

Or install it directly from `the source code repository on
Gitlab <https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac>`__
via:

.. code:: bash

   pip install git+https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac.git

The latter should however only be done if you want to access the
development versions.


.. _install-develop:
Installation for development
============================

Please head over to our :ref:`contributing` for
installation instruction for development.
