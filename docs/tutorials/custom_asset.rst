..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _custom-asset:
=============================================================
How to make a custom asset for STAC-Collection and STAC-Item:
=============================================================

This tutorial will demonstrate the process of creating a personalised asset for STAC-Collection and STAC-Item.

In order to create a customised asset, it is necessary to understand the composition of the `asset_properties` in both the STAC-Collection and STAC-Item.
The :class:`~tds2stac.TDS2STACIntegrator.asset_properties` has a dictionary with keys that can be used to enable or disable certain services in TDS within the STAC-Collection and STAC-Item.

Two keys associated with custom assets are `collection_custom_asset` and `item_custom_asset`.
Both situations use two keys, which are lists of dictionaries. Each dictionary within the lists contains the following keys:

1. `key`: This key is a string used to identify the custom asset.

2. `href`: This attribute is a string that specifies the URL of the custom asset.

3. `title`: This key is a string that specifies the custom asset's title.

4. The `roles` key is a list that specifies the custom asset's roles.

   .. note::
      The `roles` key is optional. If it is not specified, the default value is `["data"]`. For more information
      refer to the `roless <https://pystac.readthedocs.io/en/latest/api/asset.html>`_ section of the PySTAC documentation.

   .. note::
      The `roles` key is a list. If you want to specify multiple roless, you can use the following format:

      .. code-block:: javascript

         "roles": ["roles1", "roles2", "roles3", ...]

5. The `media_type` key is a string that specifies the media type of the custom asset.

   .. note::
      The `media_type` key is optional. If it is not specified, the default value is `None`. For more information
      refer to the `Media Type <https://pystac.readthedocs.io/en/latest/api/asset.html>`_ section of the PySTAC documentation.


For example, if we want to make a custom asset for STAC-Collection and STAC-Item, we can use following dictionary:

.. code-block:: javascript

    asset_properties = {
                "collection_custom_asset": [
                    {
                        "key": "key1",
                        "href": "href1",
                        "title": "title1",
                        "roles": ["roles1"],
                        "media_type": "media_type1",
                    },
                    {
                        "key": "key2",
                        "href": "href2",
                        "title": "title2",
                        "roles": ["roles2"],
                        "media_type": "media_type2",
                    },
                    {
                        "key": "key3",
                        "href": "href3",
                        "title": "title3",
                        "roles": ["roles3"],
                        "media_type": "media_type3",
                    }
                ],
                "item_custom_asset": [
                    {
                        "key": "key1",
                        "href": "href1",
                        "title": "title1",
                        "roles": ["roles1"],
                        "media_type": "media_type1",
                    },
                    {
                        "key": "key2",
                        "href": "href2",
                        "title": "title2",
                        "roles": ["roles2"],
                        "media_type": "media_type2",
                    },
                    {
                        "key": "key3",
                        "href": "href3",
                        "title": "title3",
                        "roles": ["roles3"],
                        "media_type": "media_type3",
                    }
                ]
            }
