..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _extra_metadata:
========================================
How to create `extra_metadata.json` file
========================================


The dictionary has two primary keys, namely `collection` and `item`, which might have additional subkeys based on the definitions of `pystac.Collection <https://pystac.readthedocs.io/en/latest/api/collection.html>`_ and `pystac.Item <https://pystac.readthedocs.io/en/latest/api/item.html>`_.

Each collection in the STAC-Collection JSON file can have `keywords` and `providers` as list, and `licence` as a string at the top level. To incorporate additional fields into the STAC-Collection JSON file, we can append them to the `extra_fields` dictionary. The addition of `extra_fields` is possible within the Metadata section of the STAC-Collection JSON file.

To include `pystac.common_metadata <https://pystac.readthedocs.io/en/latest/api/common_metadata.html>`_ attributes in the STAC-Item JSON file, we can add them to the top level of each `item`. To incorporate additional fields into the STAC-Item JSON file, we can append them to the `properties` dictionary. The Metadata part of the STAC-Item JSON file allows for the inclusion of `properties`. It is important to mention that the `extra_fields` parameter can also be utilised to include more fields in the STAC-Item JSON file.

.. code-block:: javascript

    {
        "collection": {
            "extra_fields":{
                "anything": "anything",
                "version": ""
            },
            "keywords": ["something"],
            "license": "something",
            "providers": []

        },
        "item": {
            "extra_fields":{
                "anything": "anything",
                "something_else": "something"
            },
            "properties":{
                "anything": "anything",
                "something_else": "something"
            },
            "created": "It will fill out automatically if set extra_metadata to True",
            "description": "",
            "end_datetime": "It will fill out automatically if set extra_metadata to True",
            "geometry": {},
            "gsd": "",
            "instruments": [],
            "license": "",
            "platform": "",
            "providers": [],
            "start_datetime": "It will fill out automatically if set extra_metadata to True",
            "title": "",
            "updated": "It will fill out automatically if set extra_metadata to True",
        }
    }
