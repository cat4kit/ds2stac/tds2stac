..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _tutorials:

======================
Tutorials
======================

This tutorial aims to demonstrate the utilization of TDS2STAC. The data sources to be utilized in this study include the following:

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Tutorials

   tag_config
   custom_extension
   extra_metadata
   custom_asset
