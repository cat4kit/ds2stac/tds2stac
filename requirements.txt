# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

urllib3==1.26.6
versioneer==0.29
pystac==1.8.3
lxml==4.9.3
requests==2.31.0
pytz==2023.3
tqdm==4.66.1
shapely==2.0.1
pypgstac==0.7.10
psycopg==3.1.10
psycopg-pool==3.1.7
