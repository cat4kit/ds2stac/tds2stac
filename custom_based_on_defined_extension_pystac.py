# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0
from pystac.extensions.scientific import Publication, ScientificExtension


class Scientific:
    """
    a class-based custom extension
    for the item via the defined extension
    in pystac
    """

    def item(self, item, harvesting_vars):
        item_publication = []
        item_publication = [
            Publication(
                harvesting_vars["publications"][0],
                harvesting_vars["publications"][1],
            )
        ]
        scientific = ScientificExtension.ext(item, add_if_missing=True)
        scientific.apply(
            doi=harvesting_vars["doi"],
            citation=harvesting_vars["citation"],
            publications=item_publication,
        )


def item(item, harvesting_vars):
    """
    a function-based custom extension
    for the item via the defined extension
    in pystac
    """
    item_publication = []
    item_publication = [
        Publication(
            harvesting_vars["publications"][0],
            harvesting_vars["publications"][1],
        )
    ]
    scientific = ScientificExtension.ext(item, add_if_missing=True)
    scientific.apply(
        doi=harvesting_vars["doi"],
        citation=harvesting_vars["citation"],
        publications=item_publication,
    )


# from tds2satc import TDS2STACIntegrator

# TDS2STACIntegrator(
#     "https://thredds.imk-ifu.kit.edu/thredds/catalog/regclim/raster/global/era5/sfc/single/daily/catalog.html",
#     stac_dir="/Users/hadizadeh-m/stac/",
#     limited_number=2,
#     depth_number=1,
#     webservice_properties={
#         "webservice": "all",
#         "web_service_config_file": "./tag_example.json",
#     },
#     extension_properties={
#         "item_extensions": [
#             "common_metadata",
#             "item_datacube_extension",
#             ("scientific_extension", "item"),
#             (
#                 "contact_extension",
#                 "item",
#                 "/Users/hadizadeh-m/dev/20231102/tds2stac/custom_based_on_none_defined_extension_pystac.py",
#             ),
#         ],
#     },
#     asset_properties={
#         "item_thumbnail": True,
#         "item_getminmax_thumbnail": True,
#         "explore_data": True,
#         "verify_explore_data": True,
#         "jupyter_notebook": True,
#         "collection_thumbnail": "wms",
#         "assets_list": ["wms", "wcs", "wfs"],
#     },
# )
