# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

image: python:3.10

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"


cache:
  paths:
    - .cache/pip
before_script:
  # replace git internal paths in order to use the CI_JOB_TOKEN
  - apt-get update -y && apt-get install -y python3 pandoc graphviz git
  - ln -s /usr/bin/python3 /usr/bin/python
  - update-alternatives --install /usr/bin/python python /usr/bin/python3 1
  - wget https://bootstrap.pypa.io/get-pip.py
  - python get-pip.py
  - python -m pip install -U pip
  - alias pip=pip3
  - alias python=python3
  - mkdir -p /usr/local/tomcat/content/thredds/public/testdata/unittestdata
  - mkdir -p /usr/local/tomcat/content/thredds/catalogs
  - cd /builds/cat4kit/ds2stac/tds2stac/unittestdata/data/
  - find . -type f -name '*.nc' -exec cp --parents {} /usr/local/tomcat/content/thredds/public/testdata/unittestdata/ \;
  - ls /usr/local/tomcat/content/thredds/public/testdata/unittestdata/
  - cd /builds/cat4kit/ds2stac/tds2stac/
  - cp /builds/cat4kit/ds2stac/tds2stac/unittestdata/catalogs/scenario* /usr/local/tomcat/content/thredds/catalogs
  - cp /builds/cat4kit/ds2stac/tds2stac/unittestdata/catalogs/main.xml /usr/local/tomcat/content/thredds/catalogs
  - cp /builds/cat4kit/ds2stac/tds2stac/unittestdata/catalogs/threddsConfig.xml /usr/local/tomcat/content/thredds/

# find-docker-port:
#   image: alpine
#   services:
#     - docker:20.10-dind
#   script:
#     - apk add nmap
#     - nmap -sT -p- docker
# thredds-docker:
#   services:
#     - docker:20.10-dind
#   script:
#     - docker run -d --name thredds -p 8085:8080 unidata/thredds-docker:5.4
#     - internal_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' thredds)
#     - echo "Internal IP address of the container:" $internal_ip
#     - docker ps
#     - sleep 600
#     - curl http://localhost:8085/thredds/catalog/catalog.html
#     - curl http://172.18.0.2:8085/thredds/catalog/catalog.html
#     - /usr/local/tomcat/bin/catalina.sh run


test-package:
  stage: test
  script:
    - pip install build twine
    - make dist
  artifacts:
    name: python-artifacts
    paths:
      - "dist/*"
    expire_in: 7 days

test:
  stage: test
  variables:
    THREDDS_XMS_SIZE: 128m
    THREDDS_XMX_SIZE: 256m
  # tags:
  #   - webterminal
  #   - x86_64
  image: unidata/thredds-docker:5.4
  script:
    # - chmod +x+u /usr/local/tomcat/bin/startup.sh
    - echo $CI_RUNNER_DESCRIPTION
    - pwd
    - cd /usr/local/tomcat/
    - ./bin/catalina.sh start
    - sleep 60
    - curl http://localhost:8080/thredds/catalog/catalog.html
    - cd /builds/cat4kit/ds2stac/tds2stac
    - pip install -r requirements.txt
    - make dev-install
    - make test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

# test:
#   stage: test
#   script:
#     - make dev-install
#     - make test
#   coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

test-docs:
  stage: test
  script:
    - make dev-install
    - make -C docs html
    - make -C docs linkcheck
  artifacts:
    paths:
    - docs/_build


deploy-package:
  stage: deploy
  needs:
    - test-package
    - test-docs
    - test
  only:
    - main
  script:
    - pip install twine
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*



pages:
  stage: deploy
  script:
    - make dev-install
    - sphinx-build -b html docs public
  artifacts:
    paths:
    - public
  only:
  - main
